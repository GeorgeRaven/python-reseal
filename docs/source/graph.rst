.. include:: substitutions

Computational Graph
===================

.. autoclass:: fhe.nn.graph.net.Net
  :members:
